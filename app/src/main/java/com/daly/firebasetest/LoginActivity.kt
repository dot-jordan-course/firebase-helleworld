package com.daly.firebasetest

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {


    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        mButtonLogin.setOnClickListener({

            if (validation()) {
                var emailAddress = mEditTextEmail.text.toString()
                var password = mEditTextPassword.text.toString()


                mAuth.signInWithEmailAndPassword(emailAddress, password).addOnCompleteListener(this, {
                    if (it.isSuccessful) {

                        var user: FirebaseUser? = mAuth.getCurrentUser();
                        //Store my user on my local db.

                        var intent: Intent = Intent(this, HomeActivity::class.java)
                        startActivity(intent)
                    }

                }).addOnFailureListener({

                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                })
            }
        }

    })

    fun validation(): Boolean {
        //First Step : Get Data
        var emailAddress = mEditTextEmail.text.toString()
        var password = mEditTextPassword.text.toString()

        //Second Step : validation

        var valided: Boolean = true


        if (password.isEmpty()) {
            mEditTextPassword.error = "Please enter your password"
            valided = false
        }
        if (emailAddress.isEmpty()) {
            mEditTextEmail.error = "Please enter your email"
            valided = false
        }

        return valided
    }
}
