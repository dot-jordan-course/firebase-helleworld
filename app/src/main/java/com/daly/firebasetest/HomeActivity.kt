package com.daly.firebasetest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser


class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        //if I aleady logged

        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            //Set on Home.
        } else {
            //Redirect to Login Page.
        }

    }
}
